# Git-flow

Projeto tem como objetivo trabalhar aspectos referente ao fluxo do GIT e a pequenas refatorações usando principíos básicos do Clean Code.

A implementação se refere a uma calculadora de N valores para somente uma operação.

Por exemplo: Informo "SOMAR" e informo os valores separados por vírgula: 10,2,5 resultado será: 17.

# Conventional Commits:
    https://www.conventionalcommits.org/en/v1.0.0/

# Semantic Versioning
    https://semver.org/